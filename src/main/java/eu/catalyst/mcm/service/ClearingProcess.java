package eu.catalyst.mcm.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import eu.catalyst.mcm.payload.MarketAction;
import eu.catalyst.mcm.payload.MarketActionCounterOffer;
import eu.catalyst.mcm.payload.PayloadMEMO2MCMNotFlex;
import eu.catalyst.mcm.payload.PrioritisedAction;

@Stateless
@Path("/clearingProcess/")
public class ClearingProcess {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ClearingProcess.class);

	public ClearingProcess() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public PayloadMEMO2MCMNotFlex clearingProcess(PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {
		log.debug("-> clearingProcess");

		PayloadMEMO2MCMNotFlex returnPayloadMEMO2MCMNotFlex = new PayloadMEMO2MCMNotFlex();
		returnPayloadMEMO2MCMNotFlex
				.setInformationBrokerServerUrl(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl());
		returnPayloadMEMO2MCMNotFlex.setTokenMEMO(inputPayloadMEMO2MCMNotFlex.getTokenMEMO());
		returnPayloadMEMO2MCMNotFlex.setTokenMCM(inputPayloadMEMO2MCMNotFlex.getTokenMCM());
		returnPayloadMEMO2MCMNotFlex
				.setMarketClearingManagerUrl(inputPayloadMEMO2MCMNotFlex.getMarketClearingManagerUrl());
		returnPayloadMEMO2MCMNotFlex.setInformationBrokerId(inputPayloadMEMO2MCMNotFlex.getInformationBrokerId());
		// --------------------------------------------------------------------------

		ArrayList<ExtendedMarketAction> bidsActionList = new ArrayList<ExtendedMarketAction>();
		ArrayList<ExtendedMarketAction> offersActionList = new ArrayList<ExtendedMarketAction>();
		ArrayList<ExtendedMarketAction> notValidActionList = new ArrayList<ExtendedMarketAction>();
		ArrayList<MarketActionCounterOffer> marketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();
		for (Iterator<PrioritisedAction> iterator = inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList()
				.iterator(); iterator.hasNext();) {

			// Filtering MarketAction by Status ("valid") and Splitting by ActionStatus
			// ("bid"/"offer")
			PrioritisedAction myPrioritisedAction = iterator.next();
			MarketAction myMarketAction = myPrioritisedAction.getMarketaction();
			Integer myPriority = myPrioritisedAction.getPriority();

			// if (market.getActionStatusid().getId() == 2) { // 2== "valid"
			if (myMarketAction.getStatusid() == 2) { // 2== "valid"
				ExtendedMarketAction myExtendedMarketAction = new ExtendedMarketAction();
				myExtendedMarketAction.marketAction = myMarketAction;
				// myAction.marketAction.getActionStatusid().setId(9);
				// myAction.marketAction.getActionStatusid().setStatus("rejected");
				myExtendedMarketAction.marketAction.setStatusid(10);
				myExtendedMarketAction.priority = myPriority;
				myExtendedMarketAction.residualValue = myMarketAction.getValue();
				// if (market.getActionTypeid().getId() == 1) { // 1== "bid"
				if (myMarketAction.getActionTypeid() == 2) { // 2== "bid" new
					bidsActionList.add(myExtendedMarketAction);
				} else { // 1== "offer" new
					offersActionList.add(myExtendedMarketAction);
				}
			} else {
				ExtendedMarketAction myExtendedMarketAction = new ExtendedMarketAction();
				myExtendedMarketAction.marketAction = myMarketAction;
				myExtendedMarketAction.priority = myPriority;
				myExtendedMarketAction.residualValue = myMarketAction.getValue();
				notValidActionList.add(myExtendedMarketAction);
			}
		}
		// Sorting bidsActionList by price in descending order
		Comparator<ExtendedMarketAction> byPricePriorityDesc = new Comparator<ExtendedMarketAction>() {
			public int compare(ExtendedMarketAction left, ExtendedMarketAction right) {
				// se il prezzo è differente
				// si confrontano i prezzi
				// altrimenti si confrontano le priorità
				int comparePrice = right.marketAction.getPrice().compareTo(left.marketAction.getPrice());
				int comparePriority = right.priority.compareTo(left.priority);

				if (comparePrice != 0) {
					return comparePrice;
				} else {
					return comparePriority;
				}

			}
		};
		Collections.sort(bidsActionList, byPricePriorityDesc);

		// Sorting offersActionList by price in ascending order
		Comparator<ExtendedMarketAction> byPricePriorityAsc = new Comparator<ExtendedMarketAction>() {
			public int compare(ExtendedMarketAction left, ExtendedMarketAction right) {
//	        	            return Long.compare(left.marketAction.getPrice(),
//	        	                    right.marketAction.getPrice());
				//return left.marketAction.getPrice().compareTo(right.marketAction.getPrice());
				// se il prezzo è differente
				// si confrontano i prezzi
				// altrimenti si confrontano le priorità
				int comparePrice = left.marketAction.getPrice().compareTo(right.marketAction.getPrice());
				int comparePriority = left.priority.compareTo(right.priority);

				if (comparePrice != 0) {
					return comparePrice;
				} else {
					return comparePriority;
				}
			}
		};
		Collections.sort(offersActionList, byPricePriorityAsc);

		// last BID.Price = null;
		// flag terminate LOOPS = false;
		// clearing price := null;

		BigDecimal newClearingPrice = null;
		BigDecimal lastBidPrice = null;
		boolean executeLoop = true;

		Iterator<ExtendedMarketAction> iteratorBids = bidsActionList.iterator();
		ExtendedMarketAction currentBid = null;
		ExtendedMarketAction precBid = null;
		if (iteratorBids.hasNext()) {
			currentBid = iteratorBids.next();
		} else {
			executeLoop = false;
		}
		Iterator<ExtendedMarketAction> iteratorOffers = offersActionList.iterator();
		ExtendedMarketAction currentOffer = null;
		ExtendedMarketAction precOffer = null;
		if (iteratorOffers.hasNext()) {
			currentOffer = iteratorOffers.next();
		} else {
			executeLoop = false;
		}

		if (executeLoop && currentBid.marketAction.getPrice().compareTo(currentOffer.marketAction.getPrice()) < 0) {
			executeLoop = false;
		}

		while (executeLoop) {

			// If BIDi.price < OFFERj.price
			// Compute clearing price := OFFERj.Price
			// flag Terminate LOOPS := true

			if (currentBid.marketAction.getPrice().compareTo(currentOffer.marketAction.getPrice()) < 0) {
				if (currentOffer.marketAction.getPrice().compareTo(precOffer.marketAction.getPrice()) == 0) {
					newClearingPrice = currentOffer.marketAction.getPrice();
				} else if ((currentBid.marketAction.getPrice().compareTo(precBid.marketAction.getPrice()) == 0)) {
					newClearingPrice = currentBid.marketAction.getPrice();
				} else { // intersection as interval
					newClearingPrice = precOffer.marketAction.getPrice();
				}
				break;
			}

			// If BIDi.residualValue == OFFERj.residualValue
			// last BID.Price = BIDi.Price
			// Execute Acceptance(BIDi):
			// Insert item in marketActionCounterOfferList (BIDi, OFFERj,
			// OFFERj.residualValue)
			// BIDi.residualValue := zero
			// BIDi.status := "accepted" (7)
			// next iteratorBids
			// Execute Acceptance(OFFERj):
			// OFFERj.residualValue := zero
			// OFFERj.status := "accepted" (7)
			// next iteratorOffers

//            if (currentBid.residualValue == currentOffer.residualValue) {
			if (currentBid.residualValue.equals(currentOffer.residualValue)) {

				MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();

//					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction);
//					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction);
				myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction.getId());
				myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction.getId());
				myMarketActionCounterOffer.setExchangedValue(currentOffer.residualValue);
				marketActionCounterOfferList.add(myMarketActionCounterOffer);

				// currentBid.residualValue. = 0L;
				currentBid.residualValue = new BigDecimal(0);
//					currentBid.marketAction.getActionStatusid().setId(7);
//					currentBid.marketAction.getActionStatusid().setStatus("accepted");
				currentBid.marketAction.setStatusid(7);

				// currentOffer.residualValue = 0L;
				currentOffer.residualValue = new BigDecimal(0);
//					currentOffer.marketAction.getActionStatusid().setId(7);
//					currentOffer.marketAction.getActionStatusid().setStatus("accepted");
				currentOffer.marketAction.setStatusid(7);

				if (iteratorBids.hasNext()) {
					precBid = currentBid;
					currentBid = iteratorBids.next();
				} else {
					newClearingPrice = currentOffer.marketAction.getPrice();
					break;
				}

				if (iteratorOffers.hasNext()) {
					precOffer = currentOffer;
					currentOffer = iteratorOffers.next();
					continue;
				} else {
					newClearingPrice = currentOffer.marketAction.getPrice();
					break;
				}
			}

			// If BIDi.residualValue < OFFERj.residualValue
			// last BID.Price = BIDi.Price
			// Execute Partially_Acceptance(OFFERj):
			// OFFERj.residualValue := OFFERj.residualValue - BIDi.residualValue
			// OFFERj.status := "partially_accepted" (8)
			// Execute Acceptance(BIDi):
			// Insert item in marketActionCounterOfferList (BIDi, OFFERj,
			// BIDi.residualValue)
			// BIDi.residualValue := zero
			// BIDi.status := "accepted" (7)
			// next iteratorBids
			//

			// if (currentBid.residualValue < currentOffer.residualValue) {
			if (currentBid.residualValue.compareTo(currentOffer.residualValue) < 0) {

				// currentOffer.residualValue -= currentBid.residualValue;
				currentOffer.residualValue = currentOffer.residualValue.subtract(currentBid.residualValue);

//					currentOffer.marketAction.getActionStatusid().setId(8);
//					currentOffer.marketAction.getActionStatusid().setStatus("partially_accepted");
				currentOffer.marketAction.setStatusid(9);

				MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();

//					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction);
//					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction);
				myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction.getId());
				myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction.getId());

				myMarketActionCounterOffer.setExchangedValue(currentBid.residualValue);
				marketActionCounterOfferList.add(myMarketActionCounterOffer);

//              currentBid.residualValue = 0L;
				currentBid.residualValue = new BigDecimal(0);
//					currentBid.marketAction.getActionStatusid().setId(7);
//					currentBid.marketAction.getActionStatusid().setStatus("accepted");
				currentBid.marketAction.setStatusid(7);

				if (iteratorBids.hasNext()) {
					precOffer = currentOffer;
					precBid = currentBid;
					currentBid = iteratorBids.next();
					continue;
				} else {
					newClearingPrice = currentOffer.marketAction.getPrice();
					break;
				}

			}

			// If BIDi.residualValue > OFFERj.residualValue
			// last BID.Price = BIDi.Price
			// Execute Partially_Acceptance(BIDi):
			// Insert item in marketActionCounterOfferList (BIDi, OFFERj,
			// OFFERj.residualValue)
			// BIDi.residualValue := BIDi.residualValue - OFFERj.residualValue
			// BIDi.status := "partially_accepted" (8)
			// Execute Acceptance(OFFERj):
			// OFFERj.residualValue := zero
			// OFFERj.status := "accepted" (7)
			// next iteratorOffers

//          if (currentBid.residualValue > currentOffer.residualValue) {
			if (currentBid.residualValue.compareTo(currentOffer.residualValue) > 0) {

				MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();

//					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction);
//					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction);
				myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction.getId());
				myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction.getId());

				myMarketActionCounterOffer.setExchangedValue(currentOffer.residualValue);
				marketActionCounterOfferList.add(myMarketActionCounterOffer);

				// currentBid.residualValue -= currentOffer.residualValue;
				currentBid.residualValue = currentBid.residualValue.subtract(currentOffer.residualValue);
//					currentBid.marketAction.getActionStatusid().setId(8);
//					currentBid.marketAction.getActionStatusid().setStatus("partially_accepted");
				currentBid.marketAction.setStatusid(9);

				// currentOffer.residualValue = 0L;
				currentOffer.residualValue = new BigDecimal(0);
//					currentOffer.marketAction.getActionStatusid().setId(7);
//					currentOffer.marketAction.getActionStatusid().setStatus("accepted");
				currentOffer.marketAction.setStatusid(7);

				if (iteratorOffers.hasNext()) {
					precBid = currentBid;
					precOffer = currentOffer;
					currentOffer = iteratorOffers.next();
				} else {
					newClearingPrice = currentBid.marketAction.getPrice();
					break;
				}
			}
		}

		// Merging Bids and Offers and not Valid
		ArrayList<PrioritisedAction> bidsOffersActionList = new ArrayList<PrioritisedAction>();
		for (iteratorBids = bidsActionList.iterator(); iteratorBids.hasNext();) {
			ExtendedMarketAction myExtendedMarketAction = iteratorBids.next();
			PrioritisedAction myPrioritisedAction = new PrioritisedAction();
			myPrioritisedAction.setMarketaction(myExtendedMarketAction.marketAction);
			myPrioritisedAction.setPriority(myExtendedMarketAction.priority);
			bidsOffersActionList.add(myPrioritisedAction);
		}
		for (iteratorOffers = offersActionList.iterator(); iteratorOffers.hasNext();) {
			ExtendedMarketAction myExtendedMarketAction = iteratorOffers.next();
			PrioritisedAction myPrioritisedAction = new PrioritisedAction();
			myPrioritisedAction.setMarketaction(myExtendedMarketAction.marketAction);
			myPrioritisedAction.setPriority(myExtendedMarketAction.priority);
			bidsOffersActionList.add(myPrioritisedAction);
		}
		for (iteratorOffers = notValidActionList.iterator(); iteratorOffers.hasNext();) {
			ExtendedMarketAction myExtendedMarketAction = iteratorOffers.next();
			PrioritisedAction myPrioritisedAction = new PrioritisedAction();
			myPrioritisedAction.setMarketaction(myExtendedMarketAction.marketAction);
			myPrioritisedAction.setPriority(myExtendedMarketAction.priority);
			bidsOffersActionList.add(myPrioritisedAction);
		}

		// --------------------------------------------------------------------------
		returnPayloadMEMO2MCMNotFlex.setClearingPrice(newClearingPrice);
		returnPayloadMEMO2MCMNotFlex.setMarketActionCounterOfferList(marketActionCounterOfferList);
		returnPayloadMEMO2MCMNotFlex.setPrioritisedActionsList(bidsOffersActionList);
		inputPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStatusid().setId(4); // cleared
		returnPayloadMEMO2MCMNotFlex.setMarketSession(inputPayloadMEMO2MCMNotFlex.getMarketSession());

		log.debug("<- clearingProcess");
		return returnPayloadMEMO2MCMNotFlex;
	}

	static class ExtendedMarketAction {
		MarketAction marketAction;

		Integer priority;

		BigDecimal residualValue;

		public ExtendedMarketAction() {
		}
	}
}
